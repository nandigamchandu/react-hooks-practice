import React, { useState, useEffect, Fragment } from 'react'
import axios from 'axios'

export function DataFetchErrorHandle() {
    const [data, setData] = useState({ hits: [] })
    const [query, setQuery] = useState('redux');
    const [url, setUrl] = useState(
        'http://hn.algolia.com/api/v1/search?query=redux',
    )
    const [isLoading, setIsLoading] = useState(false)
    const [isError, setIsError] = useState(false)

    useEffect(() => {
        const fetchData = async () => {
            setIsError(false)
            setIsLoading(true)
            try{
            const result = await axios(url)
            setData(result.data)
            }catch{
                setIsError(true)
            }
            setIsLoading(false)
        }

        fetchData()

    }, [url])

    return (
        <Fragment>
            <input type="text" value={query} onChange={event => setQuery(event.target.value)} />
            <button type="button" onClick={() => setUrl(`http://h.algolia.com/api/v1/search?query=${query}`)}>
                Search
            </button>
            {isError && <div>Something went wrong...</div>}
            {isLoading ? (
                <div>Loading ...</div>
            ) : (
                    <ul>
                        {data.hits.map(item => (
                            <li key={item.objectID}>
                                <a href={item.url}>{item.title}</a>
                            </li>
                        ))}
                    </ul>
                )}
        </Fragment>
    )
}