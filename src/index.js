import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {Example} from './SideEffect'
import {Example2} from './UsingUseEffect'
import {MyApp} from './HooksExamples/ManageArray'
import {StopWatch} from './ExampleUseEffect/StopWatch'
import {CustomHook} from './ExampleCustomEffect/CustomHook'
import {DataFetch} from './DataFetchExample/DataFetch'
import {DataFetchInput} from './DataFetchExample/DataFetchInput'
import {DataFetchTrigger} from './DataFetchExample/DataFetchTrigger'
import {DataFetchSetURL} from './DataFetchExample/DataFetchSetURL'
import {DataFetchLoadIndicate} from './DataFetchExample/DataFetchLoadIndicate'
import {DataFetchErrorHandle} from './DataFetchExample/DataFetchErrorHandling'
import {DataFetchWithForm} from './DataFetchExample/DataFetchWithForm'
import {DataFetchCustom} from './DataFetchExample/DataFetchCustom'
import {DataFetchReduce} from './DataFetchExample/DataFetchReducer'
import {DataFetchAbort} from './DataFetchExample/DataFetchAbort'
import {UseStateExample} from './HooksExamples/use-state-example'
import { UseReduceExample} from './HooksExamples/use-reduce-example'
import {UseReduceExample2} from './HooksExamples/use-reduce-example2'
import {UseContextExample1} from './HooksExamples/use-context-example'
ReactDOM.render(<React.Fragment>
    <p>Using class component</p>
    <Example />
    <p>Using Functional component</p>
    <Example2/>
    <p>useState Example</p>
    <MyApp/>
    <p>useEffect example</p>
    <StopWatch/>
    <p>custom hook</p>
    <CustomHook/>
    <p>Data fetching</p>
    <DataFetch />
    <p>Data fetching based on input</p>
    <DataFetchInput/>
    <p>Data fetching based on search</p>
    <DataFetchTrigger/>
    <p>Data fetching by setting URL</p>
    <DataFetchSetURL/>
    <p>Data fetching with Loading indicator</p>
    <DataFetchLoadIndicate/>
    <p>Data fetching with Error</p>
    <DataFetchErrorHandle />
    <p>Data fetching with Form</p>
    <DataFetchWithForm/>
    <p>Data fetching using custom hook</p>
    <DataFetchCustom/>    
    <p>Data fetching with reduce</p>
    <DataFetchReduce/>
    <p> Data fetching Abort</p>
    <DataFetchAbort/>
    <p>use state example</p>
    <UseStateExample />
    <p>use reduce example</p>
    <UseReduceExample />
    <p>use reduce example 2</p>
    <UseReduceExample2 />
    <p>use context example</p>
    <UseContextExample1/>
    </React.Fragment>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
